import pygame, sys
from pygame.locals import *

FPS = 25
WINDOWWIDTH = 720
WINDOWHEIGHT = 840
ISPLAYING = 0

def start():
    init()
def options():
    print 'Options'
    
#menu System
def menu():
    pygame.init()

    screen = pygame.display.set_mode((WINDOWWIDTH,WINDOWHEIGHT),0,32)
    font = pygame.font.SysFont('helvetica',24,bold=True)

    pygame.display.set_caption('Sabotage Blocks')
    pygame.draw.line(screen, (0,255,0), (150, 0), (150,840), 5)
    pygame.draw.line(screen, (0,255,0), (570, 0), (570,840), 5)


    font.set_underline(True)
    titleText = font.render('Sabotage Blocks', True, (0,255,0))
    font.set_underline(False)
    startText = font.render('Start', True, (255,255,255))
    optionsText = font.render('Options', True, (255,255,255))
    exitText = font.render('Quit', True, (255,255,255))

    menuCount = 0

    screen.blit(titleText, (160, 300))
    screen.blit(startText, (160, 500))
    screen.blit(optionsText, (160, 540))
    screen.blit(exitText, (160, 580))


    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key==K_ESCAPE:
                    pygame.quit()
                    sys.exit()
                if event.key==K_RETURN:
                    if menuCount == 0:
                        start()
                    elif menuCount == 1:
                        options()
                    else:
                        pygame.quit()
                        sys.exit()
                if event.key==K_DOWN:
                    if menuCount != 2:
                        menuCount = menuCount + 1
                if event.key==K_UP:
                    if menuCount != 0:
                        menuCount = menuCount - 1

                
            if menuCount == 0:
                startText = font.render('Start', True, (0,255,0))
                optionsText = font.render('Options', True, (255,255,255))
                exitText = font.render('Quit', True, (255,255,255))
                    
                screen.blit(startText, (160, 500))
                screen.blit(optionsText, (160, 540))
                screen.blit(exitText, (160, 580))
            elif menuCount == 1:
                startText = font.render('Start', True, (255,255,255))
                optionsText = font.render('Options', True, (0,255,0))
                exitText = font.render('Quit', True, (255,255,255))

                screen.blit(startText, (160, 500))
                screen.blit(optionsText, (160, 540))
                screen.blit(exitText, (160, 580))
            else:
                startText = font.render('Start', True, (255,255,255))
                optionsText = font.render('Options', True, (255,255,255))
                exitText = font.render('Quit', True, (0,255,0))

                screen.blit(startText, (160, 500))
                screen.blit(optionsText, (160, 540))
                screen.blit(exitText, (160, 580))
        
        pygame.display.update()


import  time, random, math
empty='.'
BOARDHEIGHT=20
BOARDWIDTH=10
BLOCKSIZE=20
WINDOWHEIGHT=720
WINDOWWIDTH=840
BOARD=[]
SCORE=0
LEVEL=0

#Setup margins to the top and sides of board
XMARGIN = int((WINDOWWIDTH - BOARDWIDTH * BLOCKSIZE) / 2)
TOPMARGIN = WINDOWHEIGHT - (BOARDHEIGHT * BLOCKSIZE) - 5

BLACK = (0,0,0)
RED = (255,0,0)
GREEN = (0,255,0)
BLUE = (0,0,255)
YELLOW = (255,255,0)


BGCOLOR=BLACK

#Dict of Colors for easy random choosing
COLORS = {'R': RED,
          'G': GREEN,
          'B': BLUE,
          'Y': YELLOW}
#Credit to tetromino tutorial for piece templates.  I did not redesign
#as this is the best implementation I could think of.

S_SHAPE_TEMPLATE = [['.....',
                     '.....',
                     '..OO.',
                     '.OO..',
                     '.....'],
                    ['.....',
                     '..O..',
                     '..OO.',
                     '...O.',
                     '.....']]

Z_SHAPE_TEMPLATE = [['.....',
                     '.....',
                     '.OO..',
                     '..OO.',
                     '.....'],
                    ['.....',
                     '..O..',
                     '.OO..',
                     '.O...',
                     '.....']]

I_SHAPE_TEMPLATE = [['..O..',
                     '..O..',
                     '..O..',
                     '..O..',
                     '.....'],
                    ['.....',
                     '.....',
                     'OOOO.',
                     '.....',
                     '.....']]

O_SHAPE_TEMPLATE = [['.....',
                     '.....',
                     '.OO..',
                     '.OO..',
                     '.....']]

J_SHAPE_TEMPLATE = [['.....',
                     '.O...',
                     '.OOO.',
                     '.....',
                     '.....'],
                    ['.....',
                     '..OO.',
                     '..O..',
                     '..O..',
                     '.....'],
                    ['.....',
                     '.....',
                     '.OOO.',
                     '...O.',
                     '.....'],
                    ['.....',
                     '..O..',
                     '..O..',
                     '.OO..',
                     '.....']]

L_SHAPE_TEMPLATE = [['.....',
                     '...O.',
                     '.OOO.',
                     '.....',
                     '.....'],
                    ['.....',
                     '..O..',
                     '..O..',
                     '..OO.',
                     '.....'],
                    ['.....',
                     '.....',
                     '.OOO.',
                     '.O...',
                     '.....'],
                    ['.....',
                     '.OO..',
                     '..O..',
                     '..O..',
                     '.....']]

T_SHAPE_TEMPLATE = [['.....',
                     '..O..',
                     '.OOO.',
                     '.....',
                     '.....'],
                    ['.....',
                     '..O..',
                     '..OO.',
                     '..O..',
                     '.....'],
                    ['.....',
                     '.....',
                     '.OOO.',
                     '..O..',
                     '.....'],
                    ['.....',
                     '..O..',
                     '.OO..',
                     '..O..',
                     '.....']]
TEMPLATEWIDTH=5
TEMPLATEHEIGHT=5
#Piece Dict for easy randomization
PIECES = {'S': S_SHAPE_TEMPLATE,
          'Z': Z_SHAPE_TEMPLATE,
          'J': J_SHAPE_TEMPLATE,
          'L': L_SHAPE_TEMPLATE,
          'I': I_SHAPE_TEMPLATE,
          'O': O_SHAPE_TEMPLATE,
          'T': T_SHAPE_TEMPLATE}

#set up all necessary configurables
def init():
   global CLOCK, SURFACE
   pygame.init() 
   CLOCK=pygame.time.Clock()
   SURFACE = pygame.display.set_mode((WINDOWWIDTH,WINDOWHEIGHT))
   pygame.display.set_caption('Block Stars')
   main()



# in method variables and main game loop
def main():
    global BOARD
    BOARD= createBoard()
    startTime=pygame.time.get_ticks()
    level=6
    lasttime=-1
    piece=getPiece()
    putPieceOnBoard(piece)
    paintBoard()
    downTime=0
    #Game Loop
    while True:
        curtime=pygame.time.get_ticks()
        #Check if block is locked into position. Don't lock for
        #1st second so that player may 
        if isDown(piece):
            checkBoardForFullRows()
            if downTime==0:
                downTime=curtime
            elif curtime>downTime+1000:
                startTime=pygame.time.get_ticks()
                piece=getPiece()
                if not piece: #Game Over
                    break #Game over call next statement after game loop
                downTime=0
            
                
        level=math.floor(SCORE/1000)
        if level>10:
            level=10
        #Calculate movespeed relative to player level.
        if curtime% (50*(10-level)) == 0 and curtime !=lasttime :
            lasttime=curtime
            piece=movePiece(piece, 'DOWN')
        #Monitor Keyboard Events, quit on quits, move on moves, ignore 
        #Other
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            
            if event.type == KEYDOWN:
                if event.key==K_ESCAPE:
                    pygame.quit()
                    sys.exit()
                if event.key == K_UP:
                    piece=rotatePiece(piece)
                if event.key == K_RIGHT:
                    piece=movePiece(piece, 'RIGHT')
                if event.key == K_LEFT:
                    piece=movePiece(piece, 'LEFT')
                if event.key == K_DOWN:
                    piece=movePiece(piece, 'DOWN')
        drawStatus(SCORE)
        pygame.display.update()
    #TODO: Make Game Over pygame
    font = pygame.font.SysFont('helvetica',36,bold=True)
    gameOverText = font.render('Game Over', True,(255,255,255))
    gameOverRect = gameOverText.get_rect()
    gameOverRect.topleft = ((WINDOWWIDTH-gameOverRect.width)/2,(WINDOWHEIGHT-gameOverRect.height)/2)
    pygame.draw.rect(SURFACE,BLACK, (gameOverRect.x, gameOverRect.y, gameOverRect.width, gameOverRect.height))
    SURFACE.blit(gameOverText,  gameOverRect)
    pygame.display.update()
    time.sleep(5)
    menu()

def checkGameOver(piece):
    pass
#Create blank board structure
def createBoard():
    pygame.draw.rect(SURFACE, GREEN, (XMARGIN - 3, TOPMARGIN - 20, (BOARDWIDTH * BLOCKSIZE) + 8, (BOARDHEIGHT * BLOCKSIZE) + 8), 5)
    board = []
    for i in range(BOARDWIDTH):
        board.append([empty] * BOARDHEIGHT)
    return board
#Call up next piece
def getPiece():
    template=random.choice(list(PIECES.keys()))
    color=random.choice(list(COLORS.keys()))
    piece = {'x':5, 
            'y':0,  
            'rotation':0,
            'color':color,  
            'template':template}
    templateSet = PIECES[piece['template']]
    pt=templateSet[piece['rotation']]
    for blockx in range(0,TEMPLATEWIDTH):
        for blocky in range(0,TEMPLATEHEIGHT):
            drawblockx=piece['x']+blockx
            drawblocky=piece['y']+blocky
            if pt[blockx][blocky] != '.':
                if not isValid(drawblockx, drawblocky):
                    return False
    putPieceOnBoard(piece, first=True)
    paintBoard()
    pygame.display.update()
    return piece
    
BLOCKUPIED_LIST=list()
#Modify board structure put char representations of piece color where they 
#Belong in board struct
def putPieceOnBoard(piece, first=False):
    global BOARD, BLOCKUPIED_LIST
    
    templateSet = PIECES[piece['template']]
    pt=templateSet[piece['rotation']]
    BLOCKUPIED_LIST=list()
    for blockx in range(0,TEMPLATEWIDTH):
        for blocky in range(0,TEMPLATEHEIGHT):
            drawblockx=piece['x']+blockx
            drawblocky=piece['y']+blocky
            if pt[blockx][blocky] != '.':
                if drawblockx< BOARDWIDTH and drawblocky< BOARDHEIGHT:
                    BOARD[piece['x']+blockx][piece['y']+blocky]=piece['color']
                    BLOCKUPIED_LIST.append((drawblockx,drawblocky))
#Alter board struct to remove a piece
def clearPiece(piece):
    global BOARD
    templateSet = PIECES[piece['template']]
    pt=templateSet[piece['rotation']]
    BLOCKUPIED_LIST=list()
    for blockx in range(0,TEMPLATEWIDTH):
        for blocky in range(0,TEMPLATEHEIGHT):
            drawblockx=piece['x']+blockx
            drawblocky=piece['y']+blocky
            if pt[blockx][blocky]!='.':            
                if drawblockx< BOARDWIDTH and drawblocky< BOARDHEIGHT:
                    BOARD[piece['x']+blockx][piece['y']+blocky]='.'
#move the piece within the board structure
def movePiece(piece, direction):
    tempx=piece['x']
    tempy=piece['y']
    if direction == 'DOWN':
        tempy=tempy+1
    elif direction == 'RIGHT':
        tempx=tempx+1
    elif direction == 'LEFT':
        tempx=tempx-1
    templateSet = PIECES[piece['template']]
    pt=templateSet[piece['rotation']]
    for x in range(0,TEMPLATEWIDTH):
        for y in range(0,TEMPLATEHEIGHT):
            if pt[x][y]!='.':
                if not isValid(tempx+x,tempy+y):
                    return piece
    
    clearPiece(piece)
    piece['x']=tempx
    piece['y']=tempy
    putPieceOnBoard(piece)
    paintBoard()
    return piece
#rotate piece by checking the template structure for the next template
# and altering the board structure in the same manner as a move
def rotatePiece(piece):
    tempx=piece['x']
    tempy=piece['y']
    templateSet = PIECES[piece['template']]
    templateNewIndex = (piece['rotation']+1)%len(templateSet)
    pt=templateSet[templateNewIndex]
    for x in range(0,TEMPLATEWIDTH):
        for y in range(0,TEMPLATEHEIGHT):
            if pt[x][y]!='.':
                if not isValid(tempx+x,tempy+y):
                    return piece
    clearPiece(piece)
    piece['rotation']=templateNewIndex
    putPieceOnBoard(piece)
    paintBoard()
    return piece
#check the validitiy of the placement of a single block.  If any block
# is invlid, the calling method knows that entire piece move is invalid
def isValid(futureblock_x, futureblock_y):
    if futureblock_x<0 or futureblock_x>= BOARDWIDTH:
        return False
    if futureblock_y<0 or futureblock_y>=BOARDHEIGHT:
        return False
    if (futureblock_x,futureblock_y) not in BLOCKUPIED_LIST:
        if BOARD[futureblock_x][futureblock_y] != empty:
            return False
    return True

#check to see if piece is on lowest row, or has a previously moved piece
#below it
def isDown(piece):
    templateSet = PIECES[piece['template']]
    pt=templateSet[piece['rotation']]
    for blockx in range(0,TEMPLATEWIDTH):
        for blocky in range(0,TEMPLATEHEIGHT):
            if pt[blockx][blocky]!='.':
                if not isValid(piece['x']+blockx, piece['y']+blocky+1):
                    return True
    return False
    
#Called upon a piece being down.  Checks the stack of pieces at the bottom
#of the board for full rows to be removed.
def checkBoardForFullRows():
    global rowPrinted
    for rowCount in range(BOARDHEIGHT):
        row=[]
        for column in BOARD:
            row.append(column[rowCount])
        if checkRowFull(row):
            removeRow(rowCount)
#Do actual row check.                
def checkRowFull(row):
    if empty in row:
        return False
    else:
        return True
#Do the row removal work.  Sleep for 1/2 second so player can see full 
#row, pop the row index to be removed out of list, revers the column
#to append to the right place, then flip it back around
def removeRow(rowIndex):
    global BOARD, SCORE
    time.sleep(0.5)
    for column in BOARD:
        column.pop(rowIndex)
        column.reverse()
        column.append(empty)
        column.reverse()
    paintBoard()
    SCORE=SCORE+100
    print SCORE
    pass
    
#loop through the board and call the drawBlock method with the correct
#color as indicated in the board structure.  Empty=black
def paintBoard():
    cindex=0
    for column in BOARD:
        rindex=0
        for row in column:
            if row=='.':
                drawBlock(cindex+XMARGIN/BLOCKSIZE,rindex+TOPMARGIN/BLOCKSIZE,BLACK)
            elif row=='R':
                drawBlock(cindex+XMARGIN/BLOCKSIZE,rindex+TOPMARGIN/BLOCKSIZE,RED)
            elif row=='G':
                drawBlock(cindex+XMARGIN/BLOCKSIZE,rindex+TOPMARGIN/BLOCKSIZE,GREEN)
            elif row=='B':
                drawBlock(cindex+XMARGIN/BLOCKSIZE,rindex+TOPMARGIN/BLOCKSIZE,BLUE)
            elif row=='Y':
                drawBlock(cindex+XMARGIN/BLOCKSIZE,rindex+TOPMARGIN/BLOCKSIZE,YELLOW)
            rindex=rindex+1
        cindex=cindex+1
    pygame.display.update()
        
#call pygame methods to draw rectangles to represent blocks
def drawBlock(blockx,blocky,color):
    pixelx = blockx*BLOCKSIZE
    pixely = blocky*BLOCKSIZE
    pygame.draw.rect(SURFACE,color, (pixelx, pixely, BLOCKSIZE-1, BLOCKSIZE-1))

#Credit to Tetromino.  I used a modified version of their score keeper    
def drawStatus(score):
    font = pygame.font.SysFont('helvetica',24,bold=True)
    
    
    scoreSurf = font.render('Score: %s' % SCORE, True, (255,255,255))
    scoreRect = scoreSurf.get_rect()
    scoreRect.topleft = (WINDOWWIDTH - 150, 20)
    pygame.draw.rect(SURFACE,BLACK, (scoreRect.x, scoreRect.y, scoreRect.width, scoreRect.height))
    SURFACE.blit(scoreSurf, scoreRect)

    
    level=math.floor(SCORE/1000)
    levelSurf = font.render('Level: %s' % level, True, (255,255,255))
    levelRect = levelSurf.get_rect()
    levelRect.topleft = (WINDOWWIDTH - 150, 50)
    pygame.draw.rect(SURFACE,BLACK, (levelRect.x, levelRect.y, levelRect.width, levelRect.height))
    SURFACE.blit(levelSurf, levelRect)
    


def checkForQuit():
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        if event.type == KEYDOWN:
            if event.key==K_ESCAPE:
                pygame.quit()
                sys.exit()


#python garbage to tell the game where to start
if __name__ == '__main__':
    menu()
